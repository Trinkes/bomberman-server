// MySrv.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#define NOME_DO_SERVICO TEXT("BomberManServer")
#define TAM 256

SERVICE_STATUS          MyServiceStatus;
SERVICE_STATUS_HANDLE   MyServiceStatusHandle;
BOOL flag;

int status;

VOID  WINAPI MyServiceStart(DWORD argc, LPTSTR *argv);
VOID  WINAPI MyServiceCtrlHandler(DWORD opcode);

void MostrarErro(LPSTR str, DWORD val);


void _tmain()
{
	SERVICE_TABLE_ENTRY   DispatchTable[] =
	{
		{ NOME_DO_SERVICO, MyServiceStart },
		{ NULL, NULL }
	};

	if (!StartServiceCtrlDispatcher(DispatchTable))
	{
		MostrarErro(TEXT(" [ServNT] StartServiceCtrlDispatcher error = %d\n"), GetLastError());
	}
}

void WINAPI MyServiceStart(DWORD argc, LPTSTR *argv)
{
	HANDLE File;
	TCHAR buffer[TAM];
	int n1, n2;
	DWORD nrBytes;

	flag = TRUE;

	MyServiceStatus.dwServiceType = SERVICE_WIN32;
	MyServiceStatus.dwCurrentState = SERVICE_START_PENDING;
	MyServiceStatus.dwControlsAccepted = 0;
	MyServiceStatus.dwWin32ExitCode = 0;
	MyServiceStatus.dwServiceSpecificExitCode = 0;
	MyServiceStatus.dwCheckPoint = 0;
	MyServiceStatus.dwWaitHint = 0;

	MyServiceStatusHandle = RegisterServiceCtrlHandler(
		NOME_DO_SERVICO,
		MyServiceCtrlHandler);

	if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0)
	{
		MostrarErro(TEXT(" [ServNT] RegisterServiceCtrlHandler failed %d\n"), GetLastError());
		return;
	}

	if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
	{
		status = GetLastError();
		MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
		return;
	}

	//TODO: ...

	// Initialization complete - report running status. 
	MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
	MyServiceStatus.dwCheckPoint = 0;
	MyServiceStatus.dwWaitHint = 0;
	MyServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;

	if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
	{
		status = GetLastError();
		MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
		return;
	}

	// This is where the service does its work. 
	MostrarErro(TEXT(" [ServNT] Vou iniciar o processamento \n"), 0);

	while (1) {

		Sleep(5000);

		if (flag == FALSE)
			break;

		if (MyServiceStatus.dwCurrentState == SERVICE_PAUSED)
			continue;

		if (!CreateDirectory(TEXT("c:\\temp"), NULL)){
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] CreateDirectory error %ld\n"), status);
		}

		File = CreateFile(TEXT("c:\\temp\\fich.txt"), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (File == NULL) {
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] CreateFile1 error %ld\n"), status);
			break;
		}
		if (!ReadFile(File, buffer, TAM, &nrBytes, NULL)) {
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] ReadFile error %ld\n"), status);
			CloseHandle(File);
			break;
		}
		CloseHandle(File);

		buffer[nrBytes] = 0;

		if (_stscanf_s(buffer, TEXT("%d + %d "), &n1, &n2) != 2) {
			MostrarErro(TEXT(" [ServNT] Formato n�o reconhecido ( n1 + n2 )"), 0);
		}
		else {
			File = CreateFile(TEXT("c:\\temp\\fich.txt"), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (File == NULL) {
				status = GetLastError();
				MostrarErro(" [ServNT] CreateFile2 error %ld\n", status);
				break;
			}

			if (n1 + n2 != 0)
				_stprintf_s(buffer, TAM, TEXT("%d + %d = %d\n"), n1, n2, n1 + n2);
			else {
				_stprintf_s(buffer, TAM, TEXT("Resultado = 0... vou sair!\n"));
				break;
			}

			if (!WriteFile(File, buffer, _tcslen(buffer), &nrBytes, NULL)) {
				status = GetLastError();
				MostrarErro(TEXT(" [ServNT] WriteFile error %ld\n"), status);
				CloseHandle(File);
				break;
			}
			CloseHandle(File);
		}


	}

	MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
	MyServiceStatus.dwCheckPoint = 0;
	MyServiceStatus.dwWaitHint = 0;

	if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
	{
		status = GetLastError();
		MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
	}

	return;
}

VOID WINAPI MyServiceCtrlHandler(DWORD Opcode)
{
	switch (Opcode)
	{
	case SERVICE_CONTROL_PAUSE:
		// Do whatever it takes to pause here. 
		MyServiceStatus.dwCurrentState = SERVICE_PAUSED;
		break;

	case SERVICE_CONTROL_CONTINUE:
		// Do whatever it takes to continue here. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
		break;

	case SERVICE_CONTROL_STOP:
		// Do whatever it takes to stop here. 
		MyServiceStatus.dwWin32ExitCode = 0;
		MyServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		MyServiceStatus.dwCheckPoint = 0;
		MyServiceStatus.dwWaitHint = 0;

		if (!SetServiceStatus(MyServiceStatusHandle,
			&MyServiceStatus))
		{
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
		}
		flag = FALSE;

		MostrarErro(TEXT(" [ServNT] Leaving MyService \n"), 0);
		return;

	case SERVICE_CONTROL_INTERROGATE:
		// Fall through to send current status. 
		break;

	default:
		MostrarErro(TEXT(" [ServNT] Unrecognized opcode %ld\n"),
			Opcode);
		break;
	}

	// Send current status. 
	if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
	{
		status = GetLastError();
		MostrarErro(" [ServNT] SetServiceStatus error %ld\n", status);
	}
	return;
}

HANDLE hEventSource;

void MostrarErro(LPSTR str, DWORD val)
{
	TCHAR buf[TAM * 24];
	LPSTR msg[2] = { buf, NULL };

	_stprintf_s(buf, sizeof(buf), str, val);

	if (!hEventSource) {
		hEventSource = RegisterEventSource(NULL,            // local machine
			NOME_DO_SERVICO); // source name
	}

	if (hEventSource) {
		ReportEvent(hEventSource,
			EVENTLOG_INFORMATION_TYPE,
			0,
			0,
			NULL,   // sid
			1,
			0,
			(const TCHAR **)msg,
			NULL);
	}

}
