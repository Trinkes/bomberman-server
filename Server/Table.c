#include "Table.h"

/*
-inicialize the table randomly
@table : object to inicialize
*/
void createTable(Table *t, int numberOfPlayers, int numberOfEnemys){
	srand(time(NULL));
	int r = rand();

	t->numberOfArtfacts = 0;
	//set table to blank
	for (int x = 0; x < TABLE_SIZE; x++){
		for (int y = 0; y < TABLE_SIZE; y++){
			t->table[x][y].x = x;
			t->table[x][y].y = y;
			t->table[x][y].health = 0;
			t->table[x][y].types[0] = BLANK;
			t->table[x][y].types[1] = BLANK;
			t->table[x][y].playerNumber= -1;
		}
	}

	//set players null
	for (int i = 0; i < NUMBER_OF_PLAYERS; i++){
		t->players[i].bombPower = NULL;
		t->players[i].bombs = NULL;
		t->players[i].health = NULL;
		t->players[i].number = NULL;
		t->players[i].type = BLANK;
		t->players[i].x = NULL;
		t->players[i].y = NULL;
	}

	int i = 0;
	//place players in map
	for (i = 0; i < numberOfPlayers; i++){
		placePlayer(t, PLAYER, i,i);
	}
	int enemyNumber = 0;
	//place enemys in map
	for (; i < numberOfPlayers+numberOfEnemys; i++){
		placePlayer(t, ENEMY,enemyNumber, i);
		enemyNumber++;
	}

	placeObject(NUMBER_OF_INDESTRUCTIBLE_BLOCKS, INDESTRUCTIBLE_BLOCK, t);
	placeObject(NUMBER_OF_BLOCKS, BLOCK, t);
	placeHidenObject(NUMBBER_OF_EXTRALIFE, EXTRALIFE, t);
	placeHidenObject(NUMBBER_OF_ARTFACT, ARTFACT, t);
	placeHidenObject(NUMBBER_OF_EXTRAPOWER, EXTRAPOWER, t);
	placeHidenObject(NUMBBER_OF_NUKEBOMB, NUKEBOMB, t);
	placeHidenObject(NUMBBER_OF_GATES, GATE, t);

}

/*
*place a player or enemy in map
*/
void placePlayer(Table *sharedT, ObjectTypes player, int numberOfPlayer,int pos){
	int x, y;
	switch (pos)
	{
	case 0:
		x = 0;
		y = 0;
		break;
	case 1:
		x = TABLE_SIZE-1;
		y = TABLE_SIZE-1;
		break;
	case 2:
		x = TABLE_SIZE-1;
		y = 0;
		break;
	case 3:
		x = 0;
		y = TABLE_SIZE-1;
		break;
	}
	sharedT->table[x][y].playerNumber = numberOfPlayer;
	sharedT->table[x][y].types[0] = player;

	sharedT->players[pos].bombs = 1;
	if (player == ENEMY){
		sharedT->players[pos].health = 75;
	} else{
		sharedT->players[pos].health = 10;
	}
	sharedT->players[pos].x = x;
	sharedT->players[pos].y = y;
	sharedT->players[pos].bombPower = 2;
	sharedT->players[pos].type = player;
	sharedT->players[pos].number = numberOfPlayer;
}

/*
-place an object on table randomly
@numberOfBlock : number of object to place
@type : type of the object to place
@table : where to place the objects
*/
void placeObject(int numberOfBlocks, enum ObjectTypes type, Table *t){

	while (numberOfBlocks > 0){
		int x = rand() % TABLE_SIZE;
		int y = rand() % TABLE_SIZE;

		if (t->table[x][y].types[0] == BLANK){
			t->table[x][y].types[0] = type;
			switch (type)
			{
			case BLOCK:
				t->table[x][y].health = 2;
				break;
			default:
				t->table[x][y].health = -1;
				break;
			}
			numberOfBlocks--;
		}
	}
}

/*
-place hiden objects on table radomly
@numberOfBlock : number of object to place
@type : type of the object to place
@table : where to place the objects
*/
void placeHidenObject(int numberOfObjects, ObjectTypes type, Table *table){

	int count = 0;
	int numberOfJumps = rand() % TABLE_SIZE;
	while (numberOfObjects > 0){
		for (int i = 0; i < TABLE_SIZE; i++){
			for (int j = 0; j < TABLE_SIZE; j++){
				if (table->table[i][j].types[0] == BLOCK&& table->table[i][j].types[1] == BLANK){
					count++;
					if (count >= numberOfJumps){
						table->table[i][j].types[1] = type;
						numberOfObjects--;
						if (numberOfObjects <= 0){
							return;
						}
						else{
							numberOfJumps = rand() % TABLE_SIZE;
							count = 0;
						}
					}
				}
			}
		}
	}
}


void tableToString(Table t){
	//system("cls");

 	_tprintf(TEXT("\n\n"));
	for (int y = 0; y < TABLE_SIZE; y++){
		_tprintf(TEXT("\t\t"));
		for (int x = 0; x < TABLE_SIZE; x++){
			switch (t.table[x][y].types[0]){

			case INDESTRUCTIBLE_BLOCK:
				_tprintf(TEXT("##"));
				break;
			case BLOCK:
				_tprintf(TEXT("$$"));
				break;
			case PLAYER:
				_tprintf(TEXT("P%d"), t.table[x][y].playerNumber);
				break;
			case ENEMY:
				_tprintf(TEXT("E%d"), t.table[x][y].playerNumber);
				break;
			case BLANK:
				switch (t.table[x][y].types[1]){
				case BOMB:
					_tprintf(TEXT("**"));
					break;
				case EXTRALIFE:
					_tprintf(TEXT("EL"));
					break;
				case ARTFACT:
					_tprintf(TEXT("AF"));
					break;
				case EXTRAPOWER:
					_tprintf(TEXT("EP"));
					break;
				case NUKEBOMB:
					_tprintf(TEXT("NB"));
					break;
				case GATE:
					_tprintf(TEXT("GT"));
					break;
				default:
					_tprintf(TEXT("__"));
					break;
				}

				break;
			default:
				break;
			}
		}
		_tprintf(TEXT("\n"));
	}
	_tprintf(TEXT("\n\n\n"));
}

