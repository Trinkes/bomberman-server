#include "Cell.h"
#include <time.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>
#include <stdio.h>
#include "Mensagem.h"


#define TABLE_SIZE 15
#define NUMBER_OF_INDESTRUCTIBLE_BLOCKS 20
#define NUMBER_OF_BLOCKS 75
#define NUMBER_OF_PLAYERS 4
#define NUMBER_OF_ENEMIES 2
#define NUMBBER_OF_ARTFACT 3
#define NUMBBER_OF_EXTRALIFE 5
#define NUMBBER_OF_EXTRAPOWER 2
#define NUMBBER_OF_NUKEBOMB 2
#define NUMBBER_OF_GATES 1



typedef struct {
	Cell table[TABLE_SIZE][TABLE_SIZE];
	Player players[NUMBER_OF_PLAYERS];
	BOOLEAN bomb;
	int playerNumber;
	int numberOfArtfacts;
}Table;

void createTable(Table *t, int numberOfPlayers, int numberOfEnemys);
void placeObject(int numberOfBlocks, ObjectTypes type, Table *t);
void tableToString(Table t);
void placeHidenObject(int numberOfObjects, ObjectTypes type, Table *table);
void placePlayer(Table *sharedT, ObjectTypes player, int numberOfPlayer, int pos);