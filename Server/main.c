#include <windows.h>
#include <io.h>
#include <conio.h>
#include <fcntl.h>
#include <stdio.h>

#include <aclapi.h>

#include "Actions.h"
#include <strsafe.h>
#define nullptr (0)

#define PIPE_COM TEXT("\\\\.\\pipe\\comunica")
#define PIPE_BROAD TEXT("\\\\.\\pipe\\broadcast")

#define SERVICE	TRUE

#define TABLE_MUTEX_NAME TEXT("TableMutex")
#define BUFSIZE 512
#define NOME_DO_SERVICO TEXT("BomberManServer")
#define SCORES_FILE_NAME TEXT("Scores")
#define BUFFERSIZE 5
DWORD g_BytesTransferred = 0;
typedef struct{
	int playerNumber;
	HANDLE playerHandle;
}SendDataToThread;

TCHAR SHARED_MEMORY[] = TEXT("Global\\Shared");

TCHAR szMsg[] = TEXT("Message from first process.");

DWORD WINAPI escrevePipe(LPVOID param);
int lerPipe(HANDLE cliente, Mensagem *msg, DWORD *n);
VOID WINAPI MyServiceCtrlHandler(DWORD Opcode);
void coreCicle(LPVOID *param);
void MyServiceStart(DWORD argc, LPTSTR *argv);
void MostrarErro(LPSTR str, DWORD val);
VOID WINAPI ServiceMain(_In_ DWORD  dwArgc, _In_ LPTSTR *lpszArgv);
void saveScores(HighScores *scores);
HighScores getHighScores();
void DisplayError(LPTSTR lpszFunction);

HANDLE hPipeComunica[NUMBER_OF_PLAYERS];
HANDLE hPipeBroadcast[NUMBER_OF_PLAYERS];

BOOL ret;
int pipescli = -1;
int pipeDifunde = -1;
SERVICE_STATUS          MyServiceStatus;
SERVICE_STATUS_HANDLE   MyServiceStatusHandle;

//service vars
BOOL flag;
int status;

HANDLE hThreadRecebeMSG,
hThreadEnviaMSG;

HANDLE hThreadDifunde, hEvent;

HANDLE H_Shared;
LPCTSTR pBuf;

Table tabuleiro;

Table *sharedT;

BOOLEAN gameStarted = FALSE;
int numberOfEnemys = 0;
int numberOfPlayers = 0;

HANDLE hMutex;


void Cleanup(PSID pEveryoneSID, PSID pAdminSID, PACL pACL, PSECURITY_DESCRIPTOR pSD)
{
	if (pEveryoneSID)
		FreeSid(pEveryoneSID);
	if (pAdminSID)
		FreeSid(pAdminSID);
	if (pACL)
		LocalFree(pACL);
	if (pSD)
		LocalFree(pSD);
}


void _tmain()
{
	if (SERVICE){
		SERVICE_TABLE_ENTRY   DispatchTable[] =
		{
			{ NOME_DO_SERVICO, ServiceMain },
			{ NULL, NULL }
		};

		if (!StartServiceCtrlDispatcher(DispatchTable))
		{
			MostrarErro(TEXT(" [ServNT] StartServiceCtrlDispatcher error = %d\n"), GetLastError());
		}
	}
	else{
		ServiceMain(NULL, NULL);
	}
}

VOID WINAPI ServiceMain(
	_In_ DWORD  dwArgc,
	_In_ LPTSTR *lpszArgv
	){

	HANDLE File;
	TCHAR buffer[256];
	int n1, n2;
	DWORD nrBytes;

	if (SERVICE){
		flag = TRUE;

		MyServiceStatus.dwServiceType = SERVICE_WIN32;
		MyServiceStatus.dwCurrentState = SERVICE_START_PENDING;
		MyServiceStatus.dwControlsAccepted = 0;
		MyServiceStatus.dwWin32ExitCode = 0;
		MyServiceStatus.dwServiceSpecificExitCode = 0;
		MyServiceStatus.dwCheckPoint = 0;
		MyServiceStatus.dwWaitHint = 0;

		MyServiceStatusHandle = RegisterServiceCtrlHandler(
			NOME_DO_SERVICO,
			MyServiceCtrlHandler);

		if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0)
		{
			MostrarErro(TEXT(" [ServNT] RegisterServiceCtrlHandler failed %d\n"), GetLastError());
			return;
		}

		if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
		{
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
			return;
		}


		// Initialization complete - report running status. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
		MyServiceStatus.dwCheckPoint = 0;
		MyServiceStatus.dwWaitHint = 0;

		if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
		{
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
			return;
		}

		// This is where the service does its work. 
		MostrarErro(TEXT(" [ServNT] Vou iniciar o processamento \n"), 0);


	}




	//create mutex
	hMutex = CreateMutex(NULL, FALSE, TABLE_MUTEX_NAME);

	if (hMutex == NULL)
	{
		printf("CreateMutex error: %d\n", GetLastError());
		return 1;
	}


	//inicialize pipes
	for (int i = 0; i < NUMBER_OF_PLAYERS; i++){
		hPipeBroadcast[i] = NULL;
	}

	OVERLAPPED ovl;

	int i;
#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
	_setmode(_fileno(stderr), _O_WTEXT);
#endif
	hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	ovl.hEvent = hEvent;

	SECURITY_ATTRIBUTES sa;
	PSECURITY_DESCRIPTOR pSD;
	PACL pAcl;
	EXPLICIT_ACCESS ea;
	PSID pEveryoneSID = NULL, pAdminSID = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
	pSD = (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);

	if (pSD == NULL) {
		_tprintf(TEXT("Erro LocalAlloc!!!(%d)\n"), GetLastError());
		return 0;
	}
	if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION)) {
		_tprintf(TEXT("Erro IniSec!!!(%d)\n"), GetLastError());
		return 0;
	}

	// Create a well-known SID for the Everyone group.
	if (!AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID, 0, 0, 0, 0, 0, 0, 0,
		&pEveryoneSID)){
		_tprintf(TEXT("AllocateAndInitializeSid() error %u"), GetLastError());
		Cleanup(pEveryoneSID, pAdminSID, NULL, pSD);
	}
	else{
		_tprintf(TEXT("AllocateAndInitializeSid() for the Everyone group is OK"));
		ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
		ea.grfAccessPermissions = GENERIC_READ | GENERIC_WRITE;
		ea.grfAccessMode = SET_ACCESS;
		ea.grfInheritance = SUB_CONTAINERS_AND_OBJECTS_INHERIT;
		ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
		ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
		ea.Trustee.ptstrName = (LPTSTR)pEveryoneSID;
		if (SetEntriesInAcl(1, &ea, NULL, &pAcl) != ERROR_SUCCESS) {
			_tprintf(TEXT("Erro SetAcl!!!(%d)\n"), GetLastError());
			return 0;
		}
		if (!SetSecurityDescriptorDacl(pSD, TRUE, pAcl, FALSE)) {
			_tprintf(TEXT("Erro IniSec!!!(%d)\n"), GetLastError());
			return 0;
		}
		sa.nLength = sizeof(sa);
		sa.lpSecurityDescriptor = pSD;
		sa.bInheritHandle = TRUE;
	}



	for (i = 0; i < NUMBER_OF_PLAYERS; i++){// Normal
		_tprintf(TEXT("[ESCRITOR] Vou passar � cria��o de uma c�pia do pipe Comunica  ... (CreateNamedPipe)\n"));

		hPipeComunica[i] = CreateNamedPipe(PIPE_COM,
			PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
			PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, NUMBER_OF_PLAYERS, 256, 256,
			1000, &sa);


		if (hPipeComunica[i] == INVALID_HANDLE_VALUE){
			_tperror(TEXT("Atingido limite de clientes!"));
			break;
		}
		_tprintf(TEXT("[ESCRITOR] Esperar liga��o do CLiente '%d'... (ConnectNamedPipe)\n"), i);

		ConnectNamedPipe(hPipeComunica[i], NULL);

		SendDataToThread data;
		data.playerHandle = hPipeComunica[i];
		data.playerNumber = i;

		// THREAD LER CLIENTES
		hThreadRecebeMSG = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)coreCicle, (LPVOID)&data, 0, NULL);

		if (!gameStarted){
			hPipeBroadcast[i] = CreateNamedPipe(PIPE_BROAD,
				PIPE_ACCESS_OUTBOUND | FILE_FLAG_OVERLAPPED,
				PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, NUMBER_OF_PLAYERS, 256, 256,
				1000, &sa);
			_tprintf(TEXT("[ESCRITOR] Esperar liga��o do CLiente '%d'... (ConnectNamedPipe)\n"), i);
			ConnectNamedPipe(hPipeBroadcast[i], NULL);
			numberOfPlayers++;
		}
		else{
			numberOfEnemys++;
		}
	}
	while (1);
	exit(0);
}//main

/*
*	escreve pipe normal
*	@param	argumento para mostrar mensagem
*	return
*/
DWORD WINAPI sendHighScores(LPVOID param, HighScores highscores){
	DWORD n;
	TCHAR buf[] = L"resposta";
	//int pipe = param;
	HANDLE cliente = (HANDLE)param;
	n = sizeof(HighScores);

	if (!WriteFile(cliente, &highscores, sizeof(HighScores), &n, NULL)) {
		_tperror(TEXT("[ERRO] Escrever no pipe... (WriteFile)\n"));
		exit(-1);
	}
	_tprintf(TEXT("[SERVIDOR - ESC] Enviei %s ao cliente ...\n"), buf);
	return n;
}


/*
*	read from pipe
*	@param	handle to pipe
*/
int lerPipe(HANDLE cliente, Mensagem *msg, DWORD *n){
	return ret = ReadFile(cliente, msg, sizeof(Mensagem), &n, NULL);
}

/*
*Method to handle with Player Actions
*/
void makeAction(Actions action, int playerNumber){
	if (gameStarted){

		switch (action){
		case UP:
			movePlayer(sharedT, UP, &sharedT->players[playerNumber]);
			tableToString(*sharedT);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
			break;
		case DOWN:
			movePlayer(sharedT, DOWN, &sharedT->players[playerNumber]);
			tableToString(*sharedT);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
			break;
		case LEFT:
			movePlayer(sharedT, LEFT, &sharedT->players[playerNumber]);
			tableToString(*sharedT);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
			break;
		case RIGHT:
			movePlayer(sharedT, RIGHT, &sharedT->players[playerNumber]);
			tableToString(*sharedT);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
			break;
		case PUTBOMB:
			placeBomb(&sharedT->players[playerNumber], sharedT, hPipeBroadcast, sharedT, numberOfPlayers);
			tableToString(*sharedT);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
			break;
		default:
			break;
		}
	}
	else{
		if (action == START&&gameStarted == FALSE){
			gameStarted = TRUE;

			Sleep(100);
			_tprintf(TEXT("Creating Table\n"));
			createTable(&tabuleiro, numberOfPlayers, NUMBER_OF_ENEMIES);
			_tprintf(TEXT("Table created\n"));


			H_Shared = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(Table), TEXT("MapaPartilhado"));

			if (H_Shared == INVALID_HANDLE_VALUE){
				_tprintf(TEXT("[Erro] Abrir ficheiros (%d)\n"), GetLastError());

			}

			sharedT = (Table *)MapViewOfFile(H_Shared, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(Table));


			CopyMemory((Table *)sharedT, &tabuleiro, (sizeof(Table)));


			STARTUPINFO si;
			PROCESS_INFORMATION pi;

			ZeroMemory(&si, sizeof(si));
			//o primeiro tem de ser o 0
			if (!CreateProcess(TEXT("e:\\Isec\\SO2\\Git\\Enemy\\Enemy\\Debug\\Enemy.exe"),
				TEXT("e:\\Isec\\SO2\\Git\\Enemy\\Enemy\\Debug\\Enemy.exe 0"), NULL, NULL, FALSE,
				CREATE_NEW_CONSOLE | CREATE_UNICODE_ENVIRONMENT,
				NULL, NULL,
				&si,
				&pi))
			{
				_tprintf(TEXT("Unable to execute the enemy."));
			}

			Sleep(100);

			PROCESS_INFORMATION pi1;
			STARTUPINFO si1 = { 1 };
			si1.cb = sizeof(si1);

			TCHAR cmdArgs[] = TEXT("1");
			if (!CreateProcess(TEXT("E:\\Isec\\SO2\\Git\\Enemy\\Enemy\\Debug\\Enemy.exe"),
				TEXT("e:\\Isec\\SO2\\Git\\Enemy\\Enemy\\Debug\\Enemy.exe 1"), NULL, NULL, FALSE,
				CREATE_NEW_CONSOLE,
				NULL, NULL,
				&si1,
				&pi1))
			{
				_tprintf(TEXT("Unable to execute the enemy."));
			}

			Sleep(1000);
			escrevePipeDifunde(hPipeBroadcast, sharedT, numberOfPlayers);
		}
		else if (action == HIGHSCORE){
			sendHighScores(hPipeComunica[playerNumber], getHighScores());
		}
	}

}

void coreCicle(LPVOID *param){
	DWORD n;
	Mensagem msg;
	SendDataToThread *data = (SendDataToThread*)param;
	HANDLE clientePipe = data->playerHandle;
	int playerNumber = data->playerNumber;
	DWORD dwWaitResult;
	n = sizeof(Mensagem);
	do{
		if (!lerPipe(clientePipe, &msg, &n)){
			break;
		}

		dwWaitResult = WaitForSingleObject(
			hMutex,    // handle to mutex
			INFINITE);  // no time-out interval
		switch (dwWaitResult)
		{
		case WAIT_OBJECT_0:
			makeAction(msg.action, playerNumber);
			ReleaseMutex(hMutex);
			break;
		case WAIT_ABANDONED:
		default:
			_tprintf(TEXT("Mutex Error"));
			return;
		}

	} while (1);

}


HANDLE hEventSource;

void MostrarErro(LPSTR str, DWORD val)
{
	TCHAR buf[256 * 24];
	LPSTR msg[2] = { buf, NULL };

	/*_stprintf_s(buf, sizeof(buf), str, val);

	if (!hEventSource) {
	hEventSource = RegisterEventSource(NULL,            // local machine
	NOME_DO_SERVICO); // source name
	}

	if (hEventSource) {
	ReportEvent(hEventSource,
	EVENTLOG_INFORMATION_TYPE,
	0,
	0,
	NULL,   // sid
	1,
	0,
	(const TCHAR **)msg,
	NULL);
	}*/

}



VOID WINAPI MyServiceCtrlHandler(DWORD Opcode)
{
	switch (Opcode)
	{
	case SERVICE_CONTROL_PAUSE:
		exit(1);
		// Do whatever it takes to pause here. 
		MyServiceStatus.dwCurrentState = SERVICE_PAUSED;
		break;

	case SERVICE_CONTROL_CONTINUE:
		// Do whatever it takes to continue here. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
		break;

	case SERVICE_CONTROL_STOP:
		exit(1);
		// Do whatever it takes to stop here. 
		MyServiceStatus.dwWin32ExitCode = 0;
		MyServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		MyServiceStatus.dwCheckPoint = 0;
		MyServiceStatus.dwWaitHint = 0;

		if (!SetServiceStatus(MyServiceStatusHandle,
			&MyServiceStatus))
		{
			status = GetLastError();
			MostrarErro(TEXT(" [ServNT] SetServiceStatus error %ld\n"), status);
		}
		flag = FALSE;

		MostrarErro(TEXT(" [ServNT] Leaving MyService \n"), 0);
		return;

	case SERVICE_CONTROL_INTERROGATE:
		// Fall through to send current status. 
		break;

	default:
		MostrarErro(TEXT(" [ServNT] Unrecognized opcode %ld\n"),
			Opcode);
		break;
	}

	// Send current status. 
	if (!SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus))
	{
		status = GetLastError();
		MostrarErro(" [ServNT] SetServiceStatus error %ld\n", status);
	}
	return;
}

void saveScores(HighScores *scores){

	if (scores != NULL){

		DeleteFile(SCORES_FILE_NAME);

		HANDLE hFile;
		DWORD dwBytesToWrite = (DWORD)strlen(scores);
		DWORD dwBytesWritten = 0;
		BOOL bErrorFlag = FALSE;

		hFile = CreateFile(SCORES_FILE_NAME,                // name of the write
			GENERIC_WRITE,          // open for writing
			0,                      // do not share
			NULL,                   // default security
			CREATE_NEW,             // create new file only
			FILE_ATTRIBUTE_NORMAL,  // normal file
			NULL);                  // no attr. template

		if (hFile == INVALID_HANDLE_VALUE)
		{
			DisplayError(TEXT("CreateFile"));
			_tprintf(TEXT("Terminal failure: Unable to open file \"%s\" for write.\n"), SCORES_FILE_NAME);
			return;
		}


		bErrorFlag = WriteFile(
			hFile,           // open file handle
			scores,      // start of data to write
			sizeof(HighScores),  // number of bytes to write
			&dwBytesWritten, // number of bytes that were written
			NULL);            // no overlapped structure

		if (FALSE == bErrorFlag)
		{
			DisplayError(TEXT("WriteFile"));
			printf("Terminal failure: Unable to write to file.\n");
		}
		else
		{
			if (dwBytesWritten != dwBytesToWrite)
			{
				// This is an error because a synchronous write that results in
				// success (WriteFile returns TRUE) should write all data as
				// requested. This would not necessarily be the case for
				// asynchronous writes.
				printf("Error: dwBytesWritten != dwBytesToWrite\n");
			}
			else
			{
				_tprintf(TEXT("Wrote %d bytes to %s successfully.\n"), dwBytesWritten, SCORES_FILE_NAME);
			}
		}
		CloseHandle(hFile);
	}
}


HighScores getHighScores(){


	HighScores highScores;
	HANDLE hFile;
	DWORD  dwBytesRead = 0;
	char   ReadBuffer[BUFFERSIZE] = { 0 };
	OVERLAPPED ol = { 0 };


	hFile = CreateFile(SCORES_FILE_NAME,               // file to open
		GENERIC_READ,          // open for reading
		FILE_SHARE_READ,       // share for reading
		NULL,                  // default security
		OPEN_EXISTING,         // existing file only
		FILE_ATTRIBUTE_NORMAL , // normal file
		NULL);                 // no attr. template

	if (hFile == INVALID_HANDLE_VALUE)
	{
		DisplayError(TEXT("CreateFile"));
		_tprintf(TEXT("Terminal failure: unable to open file \"%s\" for read.\n"), SCORES_FILE_NAME);
		return;
	}

	// Read one character less than the buffer size to save room for
	// the terminating NULL character. 

	if (FALSE == ReadFile(hFile, &highScores, sizeof(HighScores), &dwBytesRead, NULL))
	{
		DisplayError(TEXT("ReadFile"));
		printf("Terminal failure: Unable to read from file.\n GetLastError=%d\n", GetLastError());
		CloseHandle(hFile);
		return;
	}
	CloseHandle(hFile);
	return highScores;



}


void DisplayError(LPTSTR lpszFunction)
// Routine Description:
// Retrieve and output the system error message for the last-error code
{
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);

	lpDisplayBuf =
		(LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf)
		+ lstrlen((LPCTSTR)lpszFunction)
		+ 40) // account for format string
		* sizeof(TCHAR));

	if (FAILED(StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error code %d as follows:\n%s"),
		lpszFunction,
		dw,
		lpMsgBuf)))
	{
		printf("FATAL ERROR: Unable to output error code.\n");
	}

	_tprintf(TEXT("ERROR: %s\n"), (LPCTSTR)lpDisplayBuf);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);

}