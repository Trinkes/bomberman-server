#include <windows.h>
#include "Table.h"

#define TIME_TO_BOMBS_EXPLODE 3000

typedef struct{
	Table *table;
	Player *player;
	int x;
	int y;
	HANDLE hPipeBroadcast[4];
	int N;
}DataToThread;

void movePlayer(Table *table, Actions direction, Player *playerObject);
void placeBomb(Player *player, Table *table,HANDLE hPipeBroadcast[], Table *sharedT, int N);
int ableToPlacePlayer(Table table, int x, int y);
DWORD explodeBomb(LPVOID *param);
int dealDmgToObject(Table *table, int x, int y, int bombPower);
void catchHidenObject(Table *table, Player *player, int x, int y);
DWORD WINAPI escrevePipeDifunde(HANDLE hPipeBroadcast[], Table *sharedT, int numberOfPlayers);
int findPlayerNumberByCoordenates(Table table, int x, int y);
void gateAction();