#include "Actions.h"

/*
-move player in some direction
player : player to move
table : table where player will be moved
direction : direction of the moviment
playerObject : pointer to the player's object
return the updated table
*/
void movePlayer(Table *table, Actions direction, Player *playerObject){
	
	int x = playerObject->x;
	int y=playerObject->y;

	switch (direction){
	case UP:
		y = y - 1;
		break;
	case DOWN:
		y = y + 1;
		break;
	case LEFT:
		x = x - 1;
		break;
	case RIGHT:
		x = x + 1;
		break;
	default:
		break;
	}

	if (isAbleToPlacePlayer(*table, x, y)){
		table->table[playerObject->x][playerObject->y].types[0] = BLANK;
		table->table[playerObject->x][playerObject->y].playerNumber = table->table[playerObject->x][playerObject->y].playerNumber = -1;
		table->table[x][y].types[0] = playerObject->type;
		table->table[x][y].playerNumber= playerObject->number;
		playerObject->x = x;
		playerObject->y = y;
		if (table->table[x][y].types[1] != BLANK){
			catchHidenObject(table, playerObject, x, y);
			if (table->table[x][y].types[1] != GATE){
				table->table[x][y].types[1] = BLANK;
			}
		}
	}
	table->bomb = FALSE;
	table->playerNumber = findPlayerNumberByCoordenates(*table,playerObject->x, playerObject->y);
}

/*
-Catch the object and buff player 
*/
void catchHidenObject(Table *table,Player *player, int x,int y){

	switch (table->table[x][y].types[1]){
	case EXTRALIFE:
		player->health++;
		break;
	case ARTFACT:
		table->numberOfArtfacts++;;
		break;
	case EXTRAPOWER:
		player->bombPower++;
		break;
	case NUKEBOMB:
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++){
			if (table->players[i].type == ENEMY){
				table->players[i].health -= 25;
			}
		}
		break;
	case GATE:
		gateAction();
		break;
	default:
		break;
	}
}


void gateAction(){

}

/*
-check if is possible to place the player on position x y
@table : table where to check if is possible
@x : x of the position to check
@Y : y of the position to check
-return 0 if not possible to place the object 1 otherwise
*/
int isAbleToPlacePlayer(Table table, int x, int y){
	if (x<0 || y<0 || x>TABLE_SIZE - 1 || y>TABLE_SIZE - 1 || table.table[x][y].types[0] != BLANK||table.table[x][y].types[1] == BOMB){
		return 0;
	}else{
		return 1;
	}
}

/*
-place a bomb at player's position
@player : which places the bomb
@table : table where to place the bomb
*/
void placeBomb(Player *player, Table *table, HANDLE hPipeBroadcast[], Table *sharedT, int N){
	if (player->bombs > 0){
		table->table[player->x][player->y].types[1] = BOMB;
		player->bombs--;
		DataToThread data[1];
		data->player = player;
		data->table = table;
		data->x = player->x;
		data->y = player->y;
		data->hPipeBroadcast[0] = hPipeBroadcast[0];
		data->hPipeBroadcast[1] = hPipeBroadcast[1];
		data->hPipeBroadcast[2] = hPipeBroadcast[2];
		data->hPipeBroadcast[3] = hPipeBroadcast[3];
		data->N = N;
		HANDLE thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)explodeBomb, (LPVOID)&data, 0, NULL);
		tableToString(*table);
		table->bomb = TRUE;
		table->playerNumber = findPlayerNumberByCoordenates(*table, player->x, player->y);
	}
}

/*
function to handle with the bomb explosion
*/
DWORD explodeBomb(LPVOID *param){
	DataToThread *data = (DataToThread*)param;
	Table *table = data->table;
	Player *player = data->player;
	int x = data->x;
	int y = data->y;
	HANDLE hPipeBroadcast[4];
	hPipeBroadcast[0] = data->hPipeBroadcast[0];
	hPipeBroadcast[1] = data->hPipeBroadcast[1];
	hPipeBroadcast[2] = data->hPipeBroadcast[2];
	hPipeBroadcast[3] = data->hPipeBroadcast[3];
	int N = data->N;
	Sleep(TIME_TO_BOMBS_EXPLODE);
	table->table[x][y].types[1] = BLANK;
	player->bombs++;

	//controls when the explosion already did hit a block -0 if not, 1 otherwise
	//each element of the array will control one direction
	int objectDestroyed[4]={0, 0, 0, 0};
	for (int i = 0; i <= player->bombPower; i++){
		//has the bomb power at this "distance"/iteration
		int bombPower = player->bombPower - i + 1;
		if (objectDestroyed[0] == 0){
			objectDestroyed[0] = dealDmgToObject(table, x + i, y,bombPower);
		}
		if (objectDestroyed[1] == 0){
			objectDestroyed[1] = dealDmgToObject(table, x - i, y, bombPower);
		}
		if (objectDestroyed[2] == 0){
			objectDestroyed[2] = dealDmgToObject(table, x, y + i, bombPower);
		}
		if (objectDestroyed[3] == 0){
			objectDestroyed[3] = dealDmgToObject(table, x, y - i, bombPower);
		}
	}
	
	tableToString(*table);
	escrevePipeDifunde(hPipeBroadcast, table, N);
}


/*
-deal dmg to the object at xy position 
return 0 if there is no object to deal dmg, 1 otherwise
*/
int dealDmgToObject(Table *table, int x, int y,int bombPower){
	if (x<0 || y<0 || x>TABLE_SIZE-1 || y>TABLE_SIZE-1){
		return 1;
	}
	if (table->table[x][y].types[0] == BLOCK){
		table->table[x][y].health-=bombPower;
		if (table->table[x][y].health <= 0){
			table->table[x][y].types[0] = BLANK;
		}
		return 1;
	}else if (table->table[x][y].types[0] == INDESTRUCTIBLE_BLOCK){
		return 1;
	}
	else if (table->table[x][y].types[0] == PLAYER || table->table[x][y].types[0] == ENEMY){
		int playerIndex = findPlayerNumberByCoordenates(*table,x, y);
		if (playerIndex != -1){
			table->players[playerIndex].health --;
			if (table->players[playerIndex].health <= 0){
				table->table[table->players[playerIndex].x][table->players[playerIndex].y].types[0] = BLANK;
			}
		}
	}
	return 0;
}


int findPlayerNumberByCoordenates(Table table, int x, int y){
	for (int i = 0; i < NUMBER_OF_PLAYERS; i++){
		if (table.players[i].x == x&&table.players[i].y == y){
			return i;
		}
	}
	return -1;
}

/*
*	write on pipe
*	@param	handle to pipe
*/
DWORD WINAPI escrevePipeDifunde(HANDLE hPipeBroadcast[],Table *sharedT,int numberOfPlayers){
	DWORD n;
	int i;

	TCHAR buf[] = L"difunde";
	for (i = 0; i < numberOfPlayers; i++){
		if (hPipeBroadcast[i] != NULL){
			if (!WriteFile(hPipeBroadcast[i], sharedT, sizeof(Table), &n, NULL)) {
				_tperror(TEXT("[ERRO] Escrever no pipe... \n"));
				exit(-1);
			}
		}
	}
}