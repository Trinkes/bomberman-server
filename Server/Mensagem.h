
typedef enum
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	PUTBOMB,
	HIGHSCORE,
	START,
	FALHOU
}Actions;

typedef struct
{
	int tipo;
	char user[30];
	Actions action;
}Mensagem;

typedef struct
{
	char players[10][10];
	int score[10];
}HighScores;
