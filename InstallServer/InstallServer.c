#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#define NOME_DO_SERVICO TEXT("BomberManServer")
#define CAMINHO TEXT("E:\\Isec\\SO2\\Git\\Server\\Debug\\Server.exe")

int _tmain(int argc, TCHAR* argv[])
{
	SC_HANDLE schSCManager, schService;
	schSCManager = OpenSCManager(
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL){
		switch (GetLastError())
		{
		case ERROR_ACCESS_DENIED:
			_tprintf_s(TEXT("Erro: OpenSCManager\tERROR_ACCESS_DENIED\n"), GetLastError());
			break;
		case ERROR_DATABASE_DOES_NOT_EXIST:
			_tprintf_s(TEXT("Erro: OpenSCManager\tERROR_DATABASE_DOES_NOT_EXIST\n"), GetLastError());
		default:
			break;
		}

	}


	LPCTSTR lpszBinaryPathName = CAMINHO;

	schService = CreateService(
		schSCManager,              // SCManager database 
		NOME_DO_SERVICO,               // name of service 
		TEXT("BomberMan Server"),// service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		lpszBinaryPathName,        // service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 

	if (schService == NULL){
		switch (GetLastError())
		{
		case ERROR_ACCESS_DENIED:
			_tprintf_s(TEXT("ERROR_ACCESS_DENIED\n"));
			break;
		case ERROR_CIRCULAR_DEPENDENCY:
			_tprintf_s(TEXT("ERROR_CIRCULAR_DEPENDENCY\n"));
			break;
		case ERROR_DUPLICATE_SERVICE_NAME:
			_tprintf_s(TEXT("ERROR_DUPLICATE_SERVICE_NAME\n"));
			break;
		case ERROR_INVALID_HANDLE:
			_tprintf_s(TEXT("ERROR_INVALID_HANDLE\n"));
			break;
		case ERROR_INVALID_NAME:
			_tprintf_s(TEXT("ERROR_INVALID_NAME\n"));
			break;
		case ERROR_INVALID_PARAMETER:
			_tprintf_s(TEXT("ERROR_INVALID_PARAMETER\n"));
			break;
		case ERROR_INVALID_SERVICE_ACCOUNT:
			_tprintf_s(TEXT("ERROR_INVALID_SERVICE_ACCOUNT\n"));
			break;
		case ERROR_SERVICE_EXISTS:
			_tprintf_s(TEXT("ERROR_SERVICE_EXISTS\n"));
			break;
		case ERROR_SERVICE_MARKED_FOR_DELETE:
			_tprintf_s(TEXT("ERROR_SERVICE_MARKED_FOR_DELETE\n"));
			break;

		default:
			break;
		}

		_tprintf_s(TEXT("Error: CreateService\n"));
	}
	else
		_tprintf_s(TEXT("CreateService SUCCESS.\n"));
	
	switch (StartService(schService,NULL,NULL))
	{
	case ERROR_ACCESS_DENIED:
		_tprintf_s(TEXT("Error: ERROR_ACCESS_DENIED\n"));
		break;
	case ERROR_INVALID_HANDLE:
		_tprintf_s(TEXT("Error: ERROR_INVALID_HANDLE\n"));
		break;
	case ERROR_PATH_NOT_FOUND:
		_tprintf_s(TEXT("Error: ERROR_PATH_NOT_FOUND\n"));
		break;
	case ERROR_SERVICE_ALREADY_RUNNING:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_ALREADY_RUNNING\n"));
		break;
	case ERROR_SERVICE_DATABASE_LOCKED:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_DATABASE_LOCKED\n"));
		break;
	case ERROR_SERVICE_DEPENDENCY_DELETED:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_DEPENDENCY_DELETED\n"));
		break;
	case ERROR_SERVICE_DEPENDENCY_FAIL:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_DEPENDENCY_FAIL\n"));
		break;
	case ERROR_SERVICE_DISABLED:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_DISABLED\n"));
		break;
	case ERROR_SERVICE_LOGON_FAILED:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_LOGON_FAILED\n"));
		break;
	case ERROR_SERVICE_MARKED_FOR_DELETE:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_MARKED_FOR_DELETE\n"));
		break;
	case ERROR_SERVICE_REQUEST_TIMEOUT:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_REQUEST_TIMEOUT\n"));
		break;
	case ERROR_SERVICE_NO_THREAD:
		_tprintf_s(TEXT("Error: ERROR_SERVICE_NO_THREAD\n"));
		break;
	default:
		break;
	}
	

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
	system("sc start");
	return 0;
}
